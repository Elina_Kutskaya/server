package ru.eka.serverClient;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("Сервер ждет клиента...");

        try (Socket clientSocket = serverSocket.accept();
             InputStream inputStream = clientSocket.getInputStream();
             OutputStream outputStream = clientSocket.getOutputStream()) {
            System.out.println("Новое соединение: " + clientSocket.getInetAddress().toString());
            DataInputStream in = new DataInputStream(inputStream);
            DataOutputStream out = new DataOutputStream(outputStream);
            String line;
            while (true) {
                line = in.readUTF();
                System.out.println("Прислал клиент: " + line);
                out.writeUTF(line);
                System.out.println("Отправлено клиенту: " + line);
                out.flush();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Клиент отключился!");
    }
}
