package ru.eka.serverClient;

import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {
        try (Socket socket = new Socket("Localhost", 8080);
             InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()) {
            DataInputStream in = new DataInputStream(inputStream);
            DataOutputStream out = new DataOutputStream(outputStream);

            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
            String line;
            System.out.println("Введите сообщение: ");

            while (true) {
                line = input.readLine();
                System.out.println("Отправлено серверу: " + line);
                out.writeUTF(line);
                out.flush();
                System.out.println("Прислал сервер: " + line);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}